-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 08, 2016 at 09:25 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dgm3790final`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `user_id` int(10) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`user_id`, `user_email`, `user_pass`) VALUES
(1, 'hiro@fake.com', '1234'),
(2, 'evan@fake.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `c_id` int(25) NOT NULL,
  `f_name` varchar(100) NOT NULL,
  `l_name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `cc_num` int(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `f_name`, `l_name`, `address`, `cc_num`, `email`, `city`, `state`, `zip`, `date`) VALUES
(5, 'Sara', 'Eng', '4950 w 5100 s, 22', 666, 'utahevan@gmail.com', 'Kearns', 'Kearns', '84118', '2016-12-08 10:20:04'),
(6, 'Sara', 'Eng', '4950 w 5100 s, 22', 666, '1@3444.com', 'Kearns', 'Kearns', '84118', '2016-12-08 10:45:36'),
(7, 'Sara', 'Eng', '4950 w 5100 s, 22', 666, '1@3444.com', 'Kearns', 'Kearns', '84118', '2016-12-08 10:46:22'),
(8, 'Blerg', 'mcfatty', '4950 w 5100 s, 22', 666, 'car@lot.com', 'Kearns', 'Kearns', '84118', '2016-12-08 10:56:49'),
(9, 'Blerg', 'mcfatty', '4950 w 5100 s, 22', 666, 'car@lot.com', 'Kearns', 'Kearns', '84118', '2016-12-08 10:57:27'),
(10, 'Blerg', 'mcfatty', '4950 w 5100 s, 22', 666, 'car@lot.com', 'Kearns', 'Kearns', '84118', '2016-12-08 11:01:23'),
(11, 'Work', 'Man', '4950 w 5100 s, 22', 666, 'car2@lot.com', 'Kearns', 'Kearns', '84118', '2016-12-08 11:10:17'),
(12, 'Work', 'Man', '4950 w 5100 s, 22', 666, 'car2@lot.com', 'Kearns', 'Kearns', '84118', '2016-12-08 11:13:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`c_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `c_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
